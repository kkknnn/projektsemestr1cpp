#include "pch.h"
#include "Intro.h"
#include "gameinfo.h"

Intro::Intro()
{
	font.loadFromFile("Aerovias_Brasil_NF.ttf");
	text.setFont(font);
	textuzytkownik.setFont(font);
	text2.setFont(font);
	text3.setFont(font);
	std::string temp;
	temp = "Dzien dobry, nazywam sie profesor Dougie Howser.";
	temp = temp + '\n';
	temp = temp + "Czy pamietasz jak masz na imie?";
	text.setString(temp);
	textuzytkownik.setString(string);
	text3.setString("By kontynuowac nacisnij enter");
	text.setFillColor(sf::Color::Green);
	textuzytkownik.setFillColor(sf::Color::White);
	text2.setFillColor({ 0,0,0,0 });
	text3.setFillColor({ sf::Color::Blue });
	text.setPosition({ float(Gameinfo::SCREENRESX *0.4), 100.f });
	textuzytkownik.setPosition({ float(Gameinfo::SCREENRESX *0.4), 300.f });
	text2.setPosition({ float(Gameinfo::SCREENRESX *0.35),200.f });
	text3.setPosition({ float(Gameinfo::SCREENRESX *0.4),float(Gameinfo::SCREENRESY - 200) });
	Doctor.loadFromFile("Img/doctor.png");
	ComputerTwirl.loadFromFile("Img/intrologo.jpg");
	IntroShape.setTexture(&Doctor);
	IntroShape.setPosition({ float(Gameinfo::SCREENRESX*0.2),float(Gameinfo::SCREENRESY*0.2) });
	IntroShape.setSize({ float(Doctor.getSize().x),float(Doctor.getSize().y) });



}


Intro::~Intro()
{
}

void Intro::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(text, states);
	target.draw(textuzytkownik, states);
	target.draw(text2, states);
	target.draw(text3, states);
	target.draw(IntroShape, states);
	target.draw(IntroShape2, states);
}

void Intro::update(sf::Event & event)
{
	if (transparency == 0)
		transparencychange = -transparencychange;
	if (transparency == 255)
		transparencychange = -transparencychange;
	transparency = transparency + transparencychange;
	text3.setFillColor({ 100,100,100,sf::Uint8(transparency) });


	if (event.type == sf::Event::KeyPressed)
		if (cango)
		{																									//tlumaczenie z sf::Keyboard::Key na 
			if (event.text.unicode >= 0 && event.text.unicode < 26)										//unicode
			{
				string += (char)(event.text.unicode + 65);
				textuzytkownik.setString(string);
			}
			else if (event.text.unicode >= 26 && event.text.unicode < 36)
			{
				string += (char)(event.text.unicode + 22);
				textuzytkownik.setString(string);
			}

			else if (event.text.unicode == 59)
			{
				string.pop_back();
				textuzytkownik.setString(string);
			}
		}
	cango = false;
}


std::string Intro::returnenteredname()
{
	return string;
}

void Intro::secoundpart()
{

	text.setFillColor({ 0,0,0,0 });
	textuzytkownik.setFillColor({ 0,0,0,0 });
	text2.setFillColor({ sf::Color::Green });
	text2.setString("Witaj " + Gameinfo::PLAYERNAME + '\n' + MyFunctios::LoadStringFromFile("Story/Intro.txt"));
	text3.setString("By kontynuowac wcisnij spacje");

}

void Intro::thirdpard()
{
	text2.setFillColor({ 0,0,0,0 });
	IntroShape.setFillColor({ 0,0,0,0 });
	ComputerTwirl.setRepeated(false);
	IntroShape2.setTexture(&ComputerTwirl);
	IntroShape2.setPosition({ float(Gameinfo::SCREENRESX*0.5),float(Gameinfo::SCREENRESY*0.5) });
	IntroShape2.setSize({ float(ComputerTwirl.getSize().x),float(ComputerTwirl.getSize().y) });
	IntroShape2.setOrigin({ float(ComputerTwirl.getSize().x) / 2,float(ComputerTwirl.getSize().y / 2) });

}

void Intro::changecango()
{
	cango = true;
}
