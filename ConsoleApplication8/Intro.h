#pragma once
#include "levelintro.h"
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <string>

class Intro : public sf::Drawable
{
	sf::Text text;
	sf::Text textintro;
	sf::Text text2;
	sf::Text text3;
	sf::Text textuzytkownik;
	sf::Font font;
	sf::Texture Doctor;
	sf::Texture ComputerTwirl;
	sf::RectangleShape IntroShape;
	sf::RectangleShape IntroShape2;
	std::string string = "";
	std::string temp = "";
	int transparency{ 255 };
	int transparencychange{ 5 };
	bool cango{ true };

public:
	Intro();
	~Intro();
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	void update(sf::Event &);
	std::string returnenteredname();
	void secoundpart();
	void thirdpard();
	void changecango();
};

