﻿
#include "pch.h"
#include "map.h"
#include "object.h"
#include "menuright.h"
#include "menu.h"
#include <SFML/Graphics.hpp>    // biblioteki zewnetrzne info: https://www.sfml-dev.org/
#include <SFML/Window.hpp>
#include <vector>
#include "objectpointable.h"
#include "Intro.h"
#include "gameinfo.h"
#include "levelintro.h"
#include <iostream>
#include <fstream>
#include "objectenemy.h"

template <class T1, class T2> bool colision(T1 & A, T2 & B);

using sf::Event;


int main()
{
	srand(time(NULL));
	bool nextpart = false;
	bool thirdpart = false;
	static sf::RenderWindow GameWindow(sf::VideoMode(Gameinfo::SCREENRESX, Gameinfo::SCREENRESY, 32), "TheProgrammersRush"); // tworzy okno systemowe
	while (GameWindow.isOpen())
	{
		GameWindow.setFramerateLimit(60); // ilosc klatek / sekunda

		Intro intro;

		while (Gameinfo::GAMEPHASE == -1 && GameWindow.isOpen())							// intro
		{
			Event event;
			GameWindow.pollEvent(event);
			GameWindow.clear(sf::Color::Black);

			if (event.type == Event::Closed)											// wylaczanie gdy klikniety x 
				GameWindow.close();

			intro.update(event);

			if (!nextpart && !thirdpart) while (Keyboard::isKeyPressed(Keyboard::Key::Enter))
			{
				nextpart = true;
				Gameinfo::PLAYERNAME = intro.returnenteredname();
				intro.secoundpart();
			}
			if (nextpart) while (Keyboard::isKeyPressed(Keyboard::Key::Space))
			{
				thirdpart = true;
				intro.thirdpard();
				nextpart = false;
			}
			if (thirdpart) while (Keyboard::isKeyPressed(Keyboard::Key::Space))
				Gameinfo::GAMEPHASE = 0;


			GameWindow.draw(intro);
			GameWindow.display();
			intro.changecango();
		}


		MenuMain menu;
		while (Gameinfo::GAMEPHASE == 0 && GameWindow.isOpen())							//menu glowne
		{
			Event event;
			GameWindow.pollEvent(event);
			GameWindow.clear(sf::Color::Black);

			if (event.type == Event::Closed)											// wylaczanie gdy klikniety x 
				GameWindow.close();
			if (Keyboard::isKeyPressed(Keyboard::Key::Enter))
			{
				switch (menu.chooseoption())
				{
				case(0): {Gameinfo::GAMEPHASE = 2; break; }
				case(1): {menu.MenuOptionsStart(); break; }
				case(2): {GameWindow.close(); break; }
				}

			}
			while (sf::Keyboard::isKeyPressed(Keyboard::Key::Down))
			{
				if (menu.chooseoption() < 2)
				{
					menu.update(1);
				}
			}
			while (sf::Keyboard::isKeyPressed(Keyboard::Key::Up))
				if (menu.chooseoption() > 0)
				{
					menu.update(-1);
				}


			while (menu.getLevelofMenu() == 1 && GameWindow.isOpen())
			{
				GameWindow.pollEvent(event);
				GameWindow.clear(sf::Color::Black);

				if (event.type == Event::Closed)											// wylaczanie gdy klikniety x 
					GameWindow.close();


				while (Keyboard::isKeyPressed(Keyboard::Key::Enter))
				{
					if (menu.getGoStatus())
						switch (menu.chooseoption())
						{
						case(0): {Gameinfo::DIFFICULTY = menu.chooseoption() + 1; menu.BackToMainMenu(); break; }
						case(1): {Gameinfo::DIFFICULTY = menu.chooseoption() + 1; menu.BackToMainMenu(); break; }
						case(2): {Gameinfo::DIFFICULTY = menu.chooseoption() + 1; menu.BackToMainMenu(); break; }
						default: break;
						}
				}
				while (sf::Keyboard::isKeyPressed(Keyboard::Key::Escape))
				{
					menu.BackToMainMenu();
				}

				while (sf::Keyboard::isKeyPressed(Keyboard::Key::Right))
				{
					if (menu.chooseoption() < 2)
					{
						menu.update(1);
					}
				}
				while (sf::Keyboard::isKeyPressed(Keyboard::Key::Left))
					if (menu.chooseoption() > 0)
					{
						menu.update(-1);
					}

				menu.gochangetrue();
				GameWindow.draw(menu);
				GameWindow.display();
			}
			menu.gochangetrue();
			GameWindow.draw(menu);
			GameWindow.display();

		}


		while (Gameinfo::GAMEPHASE == 2 && GameWindow.isOpen())							//przerywnik poziomu
		{

			LevelIntro* LevelInfo = new LevelIntro();
			Event event;



			while (Gameinfo::GAMEPHASE == 2 && GameWindow.isOpen())
			{
				GameWindow.pollEvent(event);
				GameWindow.clear(sf::Color::Black);
				LevelInfo->update();
				GameWindow.draw(*LevelInfo);
				GameWindow.display();

				if (event.type == Event::Closed)
					GameWindow.close();

				if (event.type == sf::Event::KeyReleased)
				{
					GameWindow.pollEvent(event);
					if (event.key.code == Keyboard::Space)
					{
						if (Gameinfo::WHATLEVEL() < MAXLEVELS && Gameinfo::GAMEOVER == false)
						{
							Gameinfo::GAMEPHASE = 1;

						}
						else if (Gameinfo::GAMEOVER == true)
						{
							Gameinfo::RESETGAME();
							Gameinfo::GAMEPHASE = 0;
							Gameinfo::RESETLEVELSETTINGS();
						}

						else
						{
							Gameinfo::RESETGAME();
							Gameinfo::GAMEPHASE = 0;
						}
					}
				}

			}

			delete LevelInfo;



		}


		Event event;												// gra właściwa

		Maps * mapa = new Maps(Gameinfo::RETURNMAP());

		std::vector <ObjectEnemy*>vEnemies;   //vector na obiekty typu enemy

		std::vector <ObjectPointable*>vPointObjects; // vector na obiekty puntujace

		std::vector <Object>vNontranspasable;

		Object *** tablicaobiektow = new Object**[Gameinfo::SCREENRESX / Gameinfo::FIGURESIZE];		// tworzenie tablicy na obiekty planszy


		for (int i = 0; i < Gameinfo::SCREENRESX / Gameinfo::FIGURESIZE; i++)					// tworzenie pierwszego wymiaru obiektow planszy
			tablicaobiektow[i] = new Object*[Gameinfo::SCREENRESY / Gameinfo::FIGURESIZE];


		for (int i = 0; i < Gameinfo::SCREENRESX / Gameinfo::FIGURESIZE; i++)					// tworzenie obiektów planszy
			for (int j = 0; j < Gameinfo::SCREENRESY / Gameinfo::FIGURESIZE; j++)
			{
				if (mapa->ShowMapElement(j, i) == 'g' || mapa->ShowMapElement(j, i) == 'h' || mapa->ShowMapElement(j, i) == 'k' || mapa->ShowMapElement(j, i) == 'q')
				{
					char temp;

					if (mapa->ShowMapElement(j - 1, i) == 'u' || mapa->ShowMapElement(j - 1, i) == 'z' || mapa->ShowMapElement(j - 1, i) == 'n')
					{
						temp = mapa->ShowMapElement(j - 1, i);

					}
					else if (mapa->ShowMapElement(j, i - 1) == 'u' || mapa->ShowMapElement(j, i - 1) == 'z' || mapa->ShowMapElement(j, i - 1) == 'n')

					{
						temp = mapa->ShowMapElement(j, i - 1);

					}
					else if (mapa->ShowMapElement(j, i + 1) == 'u' || mapa->ShowMapElement(j, i + 1) == 'z' || mapa->ShowMapElement(j, i + 1) == 'n')
					{
						temp = mapa->ShowMapElement(j, i + 1);

					}
					else if (mapa->ShowMapElement(j + 1, i) == 'u' || mapa->ShowMapElement(j, i + 1) == 'z' || mapa->ShowMapElement(j + 1, i) == 'n')
					{
						temp = mapa->ShowMapElement(j + 1, i);

					}
					else
					{
						temp = 'u';

					}





					vPointObjects.push_back(new ObjectPointable(Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE * i), Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE *j), 1, mapa->ShowMapElement(j, i), false));
					tablicaobiektow[i][j] = new Object(Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE * i), Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE *j), 1, temp, false);
				}

				else if (mapa->ShowMapElement(j, i) == 'x' || mapa->ShowMapElement(j, i) == 'v')
				{
					vEnemies.push_back(new ObjectEnemy(int(Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE * i)), int(Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE *j)), mapa->ShowMapElement(j, i)));
					tablicaobiektow[i][j] = new Object(Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE * i), Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE *j), 1, 'u', false);
				}
				else if (mapa->ShowMapElement(j, i) == 's')
				{
					Object temp;
					temp = Object(Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE * i), Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE *j), 1, mapa->ShowMapElement(j, i), false);
					vNontranspasable.push_back(temp);
					tablicaobiektow[i][j] = new Object(Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE * i), Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE *j), 1, 'u', false);
				}
				else
					tablicaobiektow[i][j] = new Object(Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE * i), Gameinfo::FIGURESIZE / 2 + (Gameinfo::FIGURESIZE *j), 1, mapa->ShowMapElement(j, i), false);

				mapa->setLastColumnRead(i);
			}



		for (int i = 0; i < Gameinfo::SCREENRESX / Gameinfo::FIGURESIZE; i++)					// tworzenie vectora obiektow planszy przez które nie można przechodzić
			for (int j = 0; j < Gameinfo::SCREENRESY / Gameinfo::FIGURESIZE; j++)
			{
				if (!tablicaobiektow[i][j]->transpasstest())
					vNontranspasable.push_back(*tablicaobiektow[i][j]);
			}





		MenuRight menuboczne;

		Object player(25, 25, 2, 'c', true);
		player.changesize(Gameinfo::FIGURESIZE*0.8, Gameinfo::FIGURESIZE*0.8);
		Object portal(25, 25, 1, 'b', false);

		while (GameWindow.isOpen() && Gameinfo::GAMEPHASE == 1)
		{
			GameWindow.clear(sf::Color::Black);		// czyszczenie okna
			GameWindow.pollEvent(event);
			if (event.type == Event::Closed)											// wylaczanie gdy klikniety x lub ESC
				GameWindow.close();


			for (int i = 0; i < Gameinfo::SCREENRESX / Gameinfo::FIGURESIZE; i++)
				for (int j = 0; j < Gameinfo::SCREENRESY / Gameinfo::FIGURESIZE; j++)
				{
					if (tablicaobiektow[i][j]->isvisible())
					{
						tablicaobiektow[i][j]->update();
						GameWindow.draw(*tablicaobiektow[i][j]);
					}

					if (colision(player, *tablicaobiektow[i][j]))
					{
						tablicaobiektow[i][j]->updateoncolision();
					}

				}

			if (Gameinfo::WHATLEVEL() == 1)
			{
				for (int i = 0; i < vEnemies.size(); i++)
					for (int j = 0; j < vEnemies.size(); j++)
					{
						if (colision(*vEnemies[i], *vPointObjects[j]))
							vPointObjects[j]->UpdateOnCollisionEnemy();
					}

			}

			for (int i = 0; i < vEnemies.size(); i++)               // aktualizacje obiektow Enemy
			{
				vEnemies[i]->update(vNontranspasable);
				GameWindow.draw(*vEnemies[i]);

				if (colision(player, *vEnemies[i]))
				{
					vEnemies[i]->updateoncolisionplayer();
					vEnemies[i]->OnCollision();
				}
				else
				{
					vEnemies[i]->NotOnCollision();
				}
			}
			for (int i = 0; i < vPointObjects.size(); i++)                  //obiekty punktujace
			{
				vPointObjects[i]->update();
				GameWindow.draw(*vPointObjects[i]);

				if (colision(player, *vPointObjects[i]))
				{
					vPointObjects[i]->UpdateOnCollision();
					vPointObjects[i]->UpdateUserOnCollision(player);
				}
				else
				{
					vPointObjects[i]->update();
				}
				if (vPointObjects[i]->isdestroyed())
					vPointObjects.erase(vPointObjects.begin() + i);


			}


			for (int i = 0; i < vNontranspasable.size(); i++)                  // 
			{

				GameWindow.draw(vNontranspasable[i]);

				if (colision(player, vNontranspasable[i]))
				{
					vNontranspasable[i].updateoncolision();

				}
				else
				{
					vNontranspasable[i].update();
				}
			}
			player.moveuser(vNontranspasable);   // ruch gracza
			portal.update();
			GameWindow.draw(portal);
			GameWindow.draw(player);


			if (colision(player, portal))
				Gameinfo::ONPORTAL = true;
			else Gameinfo::ONPORTAL = false;

			menuboczne.update();

			GameWindow.draw(menuboczne);
			GameWindow.display();


			MyFunctios::CheckIfGameOver();   // gameover
			if (Gameinfo::GAMEOVER == true)
			{

				Gameinfo::GAMEPHASE = 2;

			}
			if (MyFunctios::CheckLevelWin()) // warunki wygranej levelu
			{
				Gameinfo::GAMEPHASE = 2;
				Gameinfo::RESETLEVELSETTINGS();
				Gameinfo::LEVELUP();

			}
			if (Gameinfo::GAMEPHASE != 1)
			{
				for (int i = 0; i < Gameinfo::SCREENRESX / Gameinfo::FIGURESIZE; i++)
					for (int j = 0; j < Gameinfo::SCREENRESY / Gameinfo::FIGURESIZE; j++)
					{
						delete tablicaobiektow[i][j];
					}
				for (int i = 0; i < Gameinfo::SCREENRESX / Gameinfo::FIGURESIZE; i++)
					delete[] tablicaobiektow[i];
				delete[]tablicaobiektow;
				delete mapa;

				for (int i = 0; i < vEnemies.size(); i++)
					delete vEnemies[i];
				for (int i = 0; i < vPointObjects.size(); i++)
					delete vPointObjects[i];

			}
		}   // gra wlasciwa // level granie


	}
	return 0;
}


template<class T1, class T2>
bool colision(T1 & A, T2 & B)
{                                                     //sprawdzanie kolizji, definicjz
	if (A.leftpos() < B.rightpos() && A.uppos() < B.downpos() && A.downpos() > B.uppos() && A.rightpos() > B.leftpos()) return true;
	return false;
}
std::string MyFunctios::LoadStringFromFile(std::string f)
{

	std::string temp = "";
	std::string odczytane = "";
	std::ifstream odczyt;
	odczyt.open(f);
	if (!odczyt.is_open())
		std::cout << "blad odczytu pliku";
	while (!odczyt.eof())
	{
		std::getline(odczyt, temp);
		odczytane = odczytane + temp + '\n';

	}
	return odczytane;

}

void MyFunctios::CheckIfGameOver()
{
	if (Gameinfo::HEALTHPOINTS <= 0)
	{
		Gameinfo::GAMEOVER = true;
		if (Gameinfo::WHATLEVEL() != 1)
			Gameinfo::GAMEOVERFLAG = 0;
		else
			Gameinfo::GAMEOVERFLAG = 2;
	}
}

bool MyFunctios::CheckLevelWin()
{
	if (Gameinfo::WHATLEVEL() == 0)
		if (Gameinfo::GETGAMEPOINTS() == 10 && Gameinfo::GETADDITIONALGOALSTAT() && Gameinfo::ONPORTAL)
			return true;
	if (Gameinfo::WHATLEVEL() == 1)
		if (Gameinfo::GETGAMEPOINTS() == 14)
			return true;
	if (Gameinfo::WHATLEVEL() == 2)
		if (Gameinfo::GETGAMEPOINTS() == 9 && Gameinfo::GETADDITIONALGOALSTAT() && Gameinfo::ONPORTAL)
			return true;
	return false;
}


