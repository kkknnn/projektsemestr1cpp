#include "pch.h"
#include "gameinfo.h"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

int Gameinfo::SCREENRESX = sf::VideoMode::getDesktopMode().width;					// inicjalizacja zmiennych globalnych
int Gameinfo::SCREENRESY = sf::VideoMode::getDesktopMode().height;
int Gameinfo::FIGURESIZE = Gameinfo::SCREENRESY/15;
int Gameinfo::GAMEPHASE = -1;
int Gameinfo::DIFFICULTY = 1;
int Gameinfo::GAMEOVERFLAG = 1;
bool Gameinfo::GAMEOVER = false;
bool Gameinfo::ONPORTAL = false;
bool Gameinfo::KEYGATHERED = false;
int Gameinfo::TIMEPENALTY = 0;
int Gameinfo::LEVELSNUM = MAXLEVELS;
int Gameinfo::GAMEPOINTS = 0;
bool Gameinfo::ADDITIONALGOAL = false;
int Gameinfo::LEVELNUM = 0;
float Gameinfo::HEALTHPOINTS = 3;
std::string Gameinfo::PLAYERNAME = "";
std::string Gameinfo::TABLEOFMAPADRESS[MAXLEVELS] = { "Maps/mapa1.txt" ,"Maps/mapa2.txt", "Maps/mapa3.txt"};
std::string Gameinfo::TABLEOFPICTURES[MAXLEVELS + 2] = { "Img/crash.png", "Img/write.png", "Img/wherehouse.png", "Img/Win.png", "Img/gameover.png" };
std::string Gameinfo::TABLEOFLEVELDESCRIPTION[MAXLEVELS + 5] = { "Story/Story1.txt","Story/Story2.txt ","Story/Story3.txt ","Story/Win.txt","Story/Gameover0.txt","Story/Gameover1.txt", "Story/Gameover2.txt"};
std::string Gameinfo::TABLEOFLEVELOBJECTIVES[MAXLEVELS] = { "Story/Obj1.txt","Story/Obj2.txt ","Story/Obj3.txt" };

void Gameinfo::SETKEYGATHERED()
{
	KEYGATHERED = true;
}
bool Gameinfo::GETKEYSTATUS()
{
	return KEYGATHERED;
}
std::string Gameinfo::GETOBJECTIVES()
{
	return TABLEOFLEVELOBJECTIVES[LEVELNUM];
}
std::string Gameinfo::GETLEVELPICTURE()
{
	if (GAMEOVER == false)
		return TABLEOFPICTURES[LEVELNUM];
	else
		return TABLEOFPICTURES[MAXLEVELS + 1];
}
void Gameinfo::SCOREPOINT(float points)
{
	GAMEPOINTS += points;
}

void Gameinfo::SETADDITIONALGOAL()
{
	ADDITIONALGOAL = true;
}

void Gameinfo::RESETLEVELSETTINGS()
{
	GAMEPOINTS = 0;
	ADDITIONALGOAL = false;
	HEALTHPOINTS = 3;
	TIMEPENALTY = 0;
	KEYGATHERED = false;
	Gameinfo::GAMEOVER = false;
}

std::string Gameinfo::RETURNMAP()
{
	return TABLEOFMAPADRESS[LEVELNUM];
}

std::string Gameinfo::RETURNDESCRIPTION()
{
	if (Gameinfo::GAMEOVER == false)
	{
		return TABLEOFLEVELDESCRIPTION[LEVELNUM];
	}
	else
		return TABLEOFLEVELDESCRIPTION[Gameinfo::LEVELSNUM + 1 + GAMEOVERFLAG];
}

void Gameinfo::LEVELUP()
{
	LEVELNUM += 1;
}

float Gameinfo::WHATLEVEL()
{
	return LEVELNUM;
}

bool Gameinfo::GETADDITIONALGOALSTAT()
{
	return ADDITIONALGOAL;
}


int Gameinfo::GETGAMEPOINTS()
{
	return GAMEPOINTS;
}

void Gameinfo::RESETGAME()
{
	LEVELNUM = 0;
	GAMEOVER = false;
}

Gameinfo::Gameinfo()
{
}

Gameinfo::~Gameinfo()
{
}

