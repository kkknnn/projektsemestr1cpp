#pragma once


#include <string>
#include <SFML/Graphics.hpp>


#define MAXLEVELS 3    // ilosc stworzonych map


namespace MyFunctios
{
	std::string LoadStringFromFile(std::string);
	void CheckIfGameOver();
	bool CheckLevelWin();
}


class Gameinfo 
{
	static int GAMEPOINTS;
	static bool KEYGATHERED;
	static bool ADDITIONALGOAL;
	static int LEVELNUM;
	static std::string TABLEOFMAPADRESS[MAXLEVELS];
	static std::string TABLEOFLEVELDESCRIPTION[MAXLEVELS + 5];
	static std::string TABLEOFPICTURES[MAXLEVELS + 2];
	static std::string TABLEOFLEVELOBJECTIVES[MAXLEVELS];
public:
	
	static void SETKEYGATHERED();
	static bool GETKEYSTATUS();
	static std::string GETOBJECTIVES();
	static std::string GETLEVELPICTURE();
	static int TIMEPENALTY;
	static bool ONPORTAL;
	static int GAMEOVERFLAG;
	static int DIFFICULTY;
	static void SCOREPOINT(float);
	static void SETADDITIONALGOAL();
	static void RESETLEVELSETTINGS();
	static std::string PLAYERNAME;
	static std::string RETURNMAP();
	static std::string RETURNDESCRIPTION();
	static void LEVELUP();
	static float WHATLEVEL();
	static bool GETADDITIONALGOALSTAT();
	static float HEALTHPOINTS;
	static int GETGAMEPOINTS();
	static int SCREENRESX;
	static int SCREENRESY;
	static int FIGURESIZE;
	static int GAMEPHASE;
    static bool GAMEOVER;
	static void RESETGAME();
	static int LEVELSNUM;
	Gameinfo();
	~Gameinfo();
};

