#include "pch.h"
#include "levelintro.h"
#include "gameinfo.h"


LevelIntro::LevelIntro()
{
	font.loadFromFile("PressStart2P.ttf");
	text.setFont(font);
	text.setFillColor({ sf::Color::Green });
	text.setCharacterSize(15);
	text.setPosition({ float(Gameinfo::SCREENRESX*0.1),float(Gameinfo::SCREENRESY / 2) });
	text.setString(MyFunctios::LoadStringFromFile(Gameinfo::RETURNDESCRIPTION()));
	texture.loadFromFile(Gameinfo::GETLEVELPICTURE());
	Picture.setTexture(&texture);
	Picture.setSize({ 500,533 });
	Picture.setPosition({ float(Gameinfo::SCREENRESX*0.35),float(Gameinfo::SCREENRESY *0.02) });
	downtext.setString("By kontynuowac nacisnij spacje");
	downtext.setFont(font);
	downtext.setPosition({ float(Gameinfo::SCREENRESX *0.3),float(Gameinfo::SCREENRESY - 200) });
	downtext.setFillColor(sf::Color::Blue);

}


LevelIntro::~LevelIntro()
{

}

void LevelIntro::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(text, states);
	target.draw(Picture, states);
	target.draw(downtext, states);
}

void LevelIntro::update()
{
	if (transparency == 0)
		transparencychange = -transparencychange;
	if (transparency == 255)
		transparencychange = -transparencychange;
	transparency = transparency + transparencychange;

	downtext.setFillColor({ 100,100,100,sf::Uint8(transparency) });
}
