#pragma once
#include <SFML/Graphics.hpp>
class LevelIntro: public sf::Drawable
{
	sf::Font font;
	sf::Text text;
	sf::Text downtext;
	sf::RectangleShape Picture;
	sf::Texture texture;
	int transparency{ 255 };
	int transparencychange{ 5 };


public:
	LevelIntro();
	~LevelIntro();
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	virtual void update();

};

