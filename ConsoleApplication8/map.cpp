#include "map.h"
#include "pch.h"
#include "map.h"
#include <string>
#include <iostream>
using std::cout;
using std::cin;
using std::string;
Maps & Maps::operator=(const Maps & temp) // operator przypisania
{
	if (this == &temp)
		return *this;

	delete[]MapScratch;

	MapHeight = temp.MapHeight;
	MapWidth = temp.MapWidth;
	MapScratch = new char*[MapHeight];
	for (int i = 0; i < MapHeight; i++)
		MapScratch[i] = new char[MapWidth];

	for (int j = 0; j < MapHeight; j++)
	{
		for (int k = 0; k < MapWidth; k++)
			MapScratch[j][k] = temp.MapScratch[j][k];
	}
	return *this;
}

Maps::Maps() : MapProjectAdress("mapa1.txt")
{
	std::ifstream odczyt;
	odczyt.open(MapProjectAdress);
	if (!odczyt.is_open())
	{
		cout << "blad oczytu pliku" << std::endl;

	}
	MapHeight = CheckMapHeight(MapProjectAdress);
	MapWidth = CheckMapWidth(MapProjectAdress);
	MapScratch = new char*[MapHeight];
	for (int i = 0; i < MapHeight; i++)
		MapScratch[i] = new char[MapWidth];

	for (int j = 0; j < MapHeight; j++)
	{
		for (int k = 0; k < MapWidth; k++)
			odczyt >> MapScratch[j][k];
	}
}


Maps::Maps(std::string H) : MapProjectAdress(H)
{

	std::ifstream odczyt;
	odczyt.open(MapProjectAdress);
	if (!odczyt.is_open())
	{
		cout << "blad oczytu pliku" << std::endl;

	}
	MapHeight = CheckMapHeight(MapProjectAdress);
	MapWidth = CheckMapWidth(MapProjectAdress);
	MapScratch = new char*[MapHeight];
	for (int i = 0; i < MapHeight; i++)
		MapScratch[i] = new char[MapWidth];

	for (int j = 0; j < MapHeight; j++)
	{
		for (int k = 0; k < MapWidth; k++)
			odczyt >> MapScratch[j][k];
	}
}
int Maps::ShowMapWidth()
{
	return MapWidth;
}
int Maps::ShowMapHeight()
{
	return MapHeight;
}
Maps::Maps(const Maps & mapa)
{
	MapHeight = mapa.MapHeight;
	MapWidth = mapa.MapWidth;
	delete MapScratch;
	MapScratch = new char*[MapHeight];
	for (int i = 0; i < MapHeight; i++)
		MapScratch[i] = new char[MapWidth];

	for (int j = 0; j < MapHeight; j++)
	{
		for (int k = 0; k < MapWidth; k++)
			MapScratch[j][k] = mapa.MapScratch[j][k];
	}
}



Maps::~Maps()
{
	for (int i = 0; i < MapHeight; i++)
		delete[] MapScratch[i];
	delete[] MapScratch;
}

char Maps::ShowMapElement(int i, int j)
{
	if (i > MapHeight) return 'z';
	if (j > MapWidth) return 'z';
	return MapScratch[i][j];
}

void Maps::setLastColumnRead(int i)
{
	LastColumnRead = i;
}

int Maps::getLastColumnRead()
{
	return LastColumnRead;
}

int Maps::CheckMapWidth(std::string adres)
{
	int liczba_znakow;
	std::ifstream odczyt;
	odczyt.open(adres);
	std::string linia;
	std::getline(odczyt, linia);
	liczba_znakow = linia.length();
	return liczba_znakow;
}

int Maps::CheckMapHeight(string adres)
{
	int * liczba_znakow = new int;
	*liczba_znakow = 0;
	std::ifstream odczyt;
	odczyt.open(adres);
	if (!odczyt.is_open())
	{
		cout << "blad oczytu pliku" << std::endl;

	}
	while (!odczyt.eof())
	{
		*liczba_znakow += 1;
		std::string temp;
		std::getline(odczyt, temp);
	}
	int temp;
	temp = *liczba_znakow;
	delete liczba_znakow;
	return temp;
}


