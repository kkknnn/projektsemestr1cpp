#pragma once
#include <string>
using std::string;
class Maps
{
public:
	Maps();
	Maps(string);
	int ShowMapWidth();
	int ShowMapHeight();
	Maps(const Maps & mapa); // konstruktor kopiujacy
	Maps & operator=(const Maps & temp); // operator przypisania
	~Maps();
	char ShowMapElement(int dim1, int dim2);
	void setLastColumnRead(int i);
	int	 getLastColumnRead();
private:
	int MapWidth;					// skladowe 
	int MapHeight;				 
	char ** MapScratch;
	string MapProjectAdress;
	int CheckMapWidth(string); // metody konstruktora
	int CheckMapHeight(string);	// metody konstruktora
	int LastColumnRead;

};