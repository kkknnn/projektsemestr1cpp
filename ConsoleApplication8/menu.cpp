#include "pch.h"
#include "menu.h"
#include "gameinfo.h"


MenuMain::MenuMain() :activeoption{ 0 }, LevelOfMenu{ 0 }
{
	font.loadFromFile("Aerovias_Brasil_NF.ttf");
	for (int i = 0; i < MENUITEMS; i++)
	{
		pos[i].setFont(font);
		pos[i].setCharacterSize(50);
		pos[i].setFillColor(sf::Color::White);
		pos[i].setPosition({ float(Gameinfo::SCREENRESX / 2),float(Gameinfo::SCREENRESY / MENUITEMS + (i * 150)) });
	}

	pos[activeoption].setFillColor(sf::Color::Green);
	pos[0].setString("Nowa Gra");
	pos[1].setString("Opcje");
	pos[2].setString("Koniec");
	info.setFont(font);
	info.setPosition({ float(Gameinfo::SCREENRESX / 2.25),float(Gameinfo::SCREENRESY / 2.5) });
	info.setString("Poziom trudnosci");
	info.setFillColor({ 100,100,100,0 });

}

void MenuMain::update(int i)
{

	if (go)
	{
		pos[activeoption].setFillColor(sf::Color::White);
		activeoption += i;
		pos[activeoption].setFillColor(sf::Color::Green);
		this->gochangefalse();
	}

}


void MenuMain::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	for (int i = 0; i < 3; i++)
		target.draw(pos[i], states);

	target.draw(info, states);

}

int MenuMain::chooseoption()
{
	return activeoption;
}

void MenuMain::MenuOptionsStart()
{
	LevelOfMenu = 1;
	for (int i = 0; i < 3; i++)
	{
		pos[i].setFillColor(sf::Color::White);
		pos[i].setPosition({ float(Gameinfo::SCREENRESX / 2.6 + (i * 150)),float(Gameinfo::SCREENRESY / 2) });
	}
	pos[activeoption].setFillColor(sf::Color::Green);
	pos[0].setString("Latwy");
	pos[1].setString("Sredni");
	pos[2].setString("Trudny");

	info.setFillColor({ 100,100,100,255 });
	gochangefalse();

}

void MenuMain::BackToMainMenu()
{
	LevelOfMenu = 0;
	info.setFillColor({ 100,100,100,0 });
	for (int i = 0; i < MENUITEMS; i++)
	{
		pos[i].setFillColor(sf::Color::White);
		pos[i].setPosition({ float(Gameinfo::SCREENRESX / 2),float(Gameinfo::SCREENRESY / MENUITEMS + (i * 150)) });
	}

	pos[activeoption].setFillColor(sf::Color::Green);
	pos[0].setString("Nowa Gra");
	pos[1].setString("Opcje");
	pos[2].setString("Koniec");
}

int MenuMain::getLevelofMenu()
{
	return LevelOfMenu;
}

void MenuMain::gochangefalse()
{
	go = false;
}

bool MenuMain::getGoStatus()
{
	return go;
}

void MenuMain::gochangetrue()
{
	go = true;
}



MenuMain::~MenuMain()
{
}
