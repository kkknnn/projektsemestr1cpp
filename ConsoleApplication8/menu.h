#pragma once
#include <SFML/Graphics.hpp>

#define MENUITEMS 3

class MenuMain: public sf::Drawable
{
private:
	sf::Font font;
	sf::Text pos[MENUITEMS];
	sf::Text info;
	int activeoption;
	int LevelOfMenu;

	bool go = true;

public:
	MenuMain();
	void update(int i);
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	int chooseoption();
	void MenuOptionsStart();
	void BackToMainMenu();
	int getLevelofMenu();
	void gochangefalse();
	bool getGoStatus();
	void gochangetrue();
	~MenuMain();
};

