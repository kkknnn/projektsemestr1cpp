#include "pch.h"
#include "menuright.h"
#include "gameinfo.h"
#include <string>


MenuRight::MenuRight()
{
	background.setSize({ float(Gameinfo::SCREENRESX - (Gameinfo::SCREENRESX - 190)),float(Gameinfo::SCREENRESY - 10) });
	background.setOutlineColor(sf::Color::White);
	background.setOutlineThickness(10);
	background.setFillColor(sf::Color::Black);
	background.setPosition({ float(Gameinfo::SCREENRESX - 190),float(Gameinfo::SCREENRESY - (Gameinfo::SCREENRESY - 10)) });
	LevelPoints.setPosition({ background.getPosition().x + 20,float(Gameinfo::SCREENRESY*0.85) });
	LevelPoints.setCharacterSize(float(background.getSize().y*0.02));
	font.loadFromFile("Aerovias_Brasil_NF.ttf");
	font2.loadFromFile("PressStart2P.ttf");
	LevelPoints.setFont(font);
	starttime = clock();
	LevelTitle.setFont(font2);
	LevelTitle.setString("Poziom " + std::to_string(Gameinfo::WHATLEVEL()+1));
	LevelTitle.setPosition({float(background.getPosition().x + 20), float(Gameinfo::SCREENRESY*0.05) });
	LevelTitle.setCharacterSize(20.f);
	LevelObjectives.setFont(font);
	LevelObjectives.setPosition({ float(background.getPosition().x + 20), float(Gameinfo::SCREENRESY*0.3) });
	LevelObjectives.setString(MyFunctios::LoadStringFromFile(Gameinfo::GETOBJECTIVES()));
	LevelObjectives.setCharacterSize(float(background.getSize().y*0.02));
	TimeLeft.setString("Pozostalo ");
	TimeLeft.setFont(font);
	TimeLeft.setPosition({ float(background.getPosition().x + 20), float(Gameinfo::SCREENRESY*0.8) });
	TimeLeft.setCharacterSize(float(background.getSize().y*0.02));
	UpHealth.setString("Zdrowie: ");
	UpHealth.setFont(font);
	UpHealth.setPosition({ float(background.getPosition().x + 20), float(Gameinfo::SCREENRESY*0.7) });
	UpHealth.setCharacterSize(float(background.getSize().y*0.02));
	HealthIcon.loadFromFile("Img/health.png");
	for (int i = 0; i < 3; i++)
	{
		PlayerHealth[i].setSize({ 30.f,30.f });
		PlayerHealth[i].setTexture(&HealthIcon);
		PlayerHealth[i].setPosition({ float(background.getPosition().x + 20 + i * 30), float(Gameinfo::SCREENRESY*0.75) });
	}
}


MenuRight::~MenuRight()
{
}

void MenuRight::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(background, states);
	target.draw(LevelPoints, states);
	target.draw(TimeLeft, states);
	target.draw(UpHealth, states);
	target.draw(LevelObjectives, states);
	target.draw(LevelTitle, states);
	for (int i = 0; i < Gameinfo::HEALTHPOINTS; i++)
	{
		target.draw(PlayerHealth[i], states);

	}
}

void MenuRight::update()
{
	punkty = Gameinfo::GETGAMEPOINTS();							//aktualizacja punkt�w
	LevelPoints.setString("Punkty: " + std::to_string(punkty));
	actualtime = (starttime - clock()) / CLOCKS_PER_SEC;
	if (int((TimePerLevel / Gameinfo::DIFFICULTY) + actualtime - Gameinfo::TIMEPENALTY) < 0)									//gameover
	{
		Gameinfo::GAMEOVER = true;
		Gameinfo::GAMEOVERFLAG = 1;
	}
	TimeLeft.setString("Pozostalo:   " + std::to_string(TimePerLevel / Gameinfo::DIFFICULTY + actualtime - Gameinfo::TIMEPENALTY) + " sekund");
}
