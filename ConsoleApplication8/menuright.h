#pragma once
#include <SFML/Graphics.hpp>
#include <time.h>

class MenuRight : public sf::Drawable
{
	#define TimePerLevel 180


	int punkty;
	int starttime;
	int actualtime;
	float menuposx=820;
	float menuposy=20;
	sf::Font font;
	sf::Font font2;
	sf::RectangleShape background;
	sf::Text LevelPoints;
	sf::Text TimeLeft;
	sf::Text UpHealth;
	sf::Text LevelObjectives;
	sf::Text LevelTitle;
	sf::RectangleShape PlayerHealth[3];
	sf::Texture HealthIcon;
	
public:
	MenuRight();
	~MenuRight();
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	void update();
};

