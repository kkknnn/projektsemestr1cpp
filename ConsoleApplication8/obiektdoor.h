#pragma once
#include "obiekt.h"
class obiektdoor :
	public obiekt
{

public:
	obiektdoor(int posx, int posy, char symbol);
	~obiektdoor();
	void updateoncolisionplayer();
	void update() override;
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;

private:

};

