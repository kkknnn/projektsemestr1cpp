#include "pch.h"
#include "object.h"
#include "gameinfo.h"
#include <SFML/Graphics.hpp>
#include "resourceholder.h"

Resources Object::resources = Resources();

Object::Object(int posx, int posy, float speed, char S, bool move)
{
	Figure.setOrigin((sizex / 2.f), (sizey / 2.f));
	Figure.setPosition(posx, posy);
	Figure.setSize({ sizex,sizey });
	symbol = S;
	movable = move;
	velocity = speed;


	if (symbol == 'u')
	{
		if (Gameinfo::WHATLEVEL() == 0)
			Figure.setTexture(&resources.getTexture("Img/carpet.jpg"));

		else if (Gameinfo::WHATLEVEL() == 2)
		{
			Figure.setTexture(&resources.getTexture("Img/basementfloor.jpg"));
		}

		else
		{
			Figure.setTexture(&resources.getTexture("Img/internet.png"));
		}
	}
	if (symbol == 'n')
	{
		Figure.setTexture(&resources.getTexture("Img/tiles.jpg"));
	}
	else if (symbol == 'y')
	{
		if (Gameinfo::WHATLEVEL() == 0)
			Figure.setTexture(&resources.getTexture("Img/wallcabinet.jpg"));
		else
		{
			Figure.setTexture(&resources.getTexture("Img/wallcabinetbasement.jpg"));
		}

		transpassing = false;
		enterable = true;
	}
	else if (symbol == 'a')
	{
		Figure.setTexture(&resources.getTexture("Img/coffeemaker.jpg"));
		transpassing = false;
		enterable = true;
	}
	else if (symbol == 'm')
	{
		Figure.setTexture(&resources.getTexture("Img/toilet.jpg"));
		transpassing = false;
		enterable = true;
	}
	else if (symbol == 'P')
	{
		Figure.setTexture(&resources.getTexture("Img/plantwood.png"));
		transpassing = false;
		enterable = true;
	}
	else if (symbol == 'O')
	{
		Figure.setTexture(&resources.getTexture("Img/plantcarpet.jpg"));
		transpassing = false;
		enterable = true;
	}
	else if (symbol == 'w')
	{
		if (Gameinfo::WHATLEVEL() != 1)
		{
			Figure.setTexture(&resources.getTexture("Img/wallnormal.jpg"));
			transpassing = false;
			enterable = true;
		}
		else
		{
			Figure.setTexture(&resources.getTexture("Img/internet2.jpg"));
			transpassing = false;
			enterable = true;
		}
	}
	else if (symbol == 'r')
	{
		Figure.setTexture(&resources.getTexture("Img/wallcabinet2.jpg"));
		transpassing = false;
		enterable = true;
	}
	else if (symbol == 'e')
	{
		if (Gameinfo::WHATLEVEL() != 1)
		{
			Figure.setTexture(&resources.getTexture("Img/wallup.jpg"));
			transpassing = false;
			enterable = true;
		}
		else
		{

			Figure.setTexture(&resources.getTexture("Img/internet2.jpg"));
			transpassing = false;
			enterable = true;
		}
	}
	else if (symbol == 't')
	{
		Figure.setTexture(&resources.getTexture("Img/tableglass.jpg"));
		transpassing = false;
		enterable = true;
	}
	else if (symbol == 'o')
	{
		Figure.setTexture(&resources.getTexture("Img/xero.jpg"));
		transpassing = false;
		enterable = true;
	}
	else if (symbol == 'p')
	{
		Figure.setTexture(&resources.getTexture("Img/chairwood.jpg"));
		transpassing = false;
		enterable = true;
	}

	else if (symbol == 'l')
	{
		Figure.setTexture(&resources.getTexture("Img/door_closed.png"));
		transpassing = false;
		enterable = true;
	}

	else if (symbol == 'c')
	{
		Figure.setTexture(&resources.getTexture("Img/playerright.png"));
	}
	else if (symbol == 'z')
	{
		if (Gameinfo::WHATLEVEL() == 2)
		{
			Figure.setTexture(&resources.getTexture("Img/woodbasement.jpg"));
		}
		else
			Figure.setTexture(&resources.getTexture("Img/wood.png"));
	}

	else if (symbol == 'b')
	{
		if (Gameinfo::WHATLEVEL() != 2)
			Figure.setTexture(&resources.getTexture("Img/computer.png"));
		else
		{
			Figure.setFillColor({ 0, 0, 0, 0 });
		}
	}

	else if (symbol == 's')
	{
		if (Gameinfo::WHATLEVEL() != 1)
		{
			Figure.setTexture(&resources.getTexture("Img/box.png"));
		}
		else
		{
			Figure.setTexture(&resources.getTexture("Img/antivir.png"));
		}
		transpassing = false;
		enterable = true;
	}

}

Object::Object()
{
}

Object::~Object()
{
}

float Object::getPositionX()
{
	return Figure.getPosition().x;
}

float Object::getPositionY()
{
	return Figure.getPosition().y;
}

void Object::setPosition(float x, float y)
{
	Figure.setPosition({ x,x });
}

char Object::getSymbol()
{
	return symbol;
}

void Object::setSpeedX(float newspeed)
{
	speedx = newspeed;
}

void Object::setSpeedY(float newspeed)
{
	speedy = newspeed;
}

void Object::setVelocity(float newvelocity)
{
	velocity = newvelocity;
}

void Object::setTranspassable(bool Val)
{
	transpassing = Val;
}

bool Object::transpasstest()
{
	return transpassing;
}

void Object::update()
{
	if (symbol == 'l' && Gameinfo::GETKEYSTATUS())
	{
		transpassing = true;
		enterable = true;
	}
	if (symbol == 's')
	{
		transpassing = true;
		movable = false;
		lastmove = 0;
	}
	if (symbol == 'b' && Gameinfo::WHATLEVEL() == 2 && Gameinfo::GETGAMEPOINTS() == 9 && Gameinfo::GETADDITIONALGOALSTAT())
	{
		Figure.setFillColor({ 0, 0, 0, 255 });
		Figure.setTexture(&resources.getTexture("Img/portal.png"));
	}




}

void Object::updateoncolision()
{
	if (symbol == 'l')
	{
		Figure.setTexture(&resources.getTexture("Img/door_open.png"));
	}
	if (symbol == 's')
	{
		movable = true;
		enterable = true;
	}
}

int Object::getLastMove()
{
	return lastmove;
}

void Object::moveuser(std::vector <Object> & A)
{

	if (movable == true)
	{
		if (Keyboard::isKeyPressed(Keyboard::Key::Left))
		{
			if (this->leftpos() < 0)
			{
				return;
			}
			for (int i = 0; i < A.size(); i++)
			{
				if (A[i].leftpos() < rightpos() && A[i].uppos() < downpos() && A[i].downpos() > uppos() && A[i].rightpos() > leftpos() - velocity)
				{
					if (!A[i].transpasstest() && A[i].getSymbol() != 's')
						return;

					if (A[i].getSymbol() == 's')
					{
						A[i].movefigure(A, this->getVelocity());
						if (A[i].CanBeEntered() == false)
							return;
					}
				}
			}

			{
				this->setSpeedX(-velocity);

				Figure.setTexture(&resources.getTexture("Img/playerleft.png"));

				this->Figure.move(speedx, speedy);
			}
		}
		else if (Keyboard::isKeyPressed(Keyboard::Key::Right))
		{
			if (this->rightpos() > float(Gameinfo::SCREENRESX - 190))
			{
				return;
			}
			for (int i = 0; i < A.size(); i++)
			{
				if (A[i].leftpos() < rightpos() + velocity && A[i].uppos() < downpos() && A[i].downpos() > uppos() && A[i].rightpos() > leftpos())
				{
					if (!A[i].transpasstest() && A[i].getSymbol() != 's')
						return;
					if (A[i].getSymbol() == 's')
					{
						A[i].movefigure(A, this->getVelocity());
						if (A[i].CanBeEntered() == false)
							return;
					}
				}
			}

			this->setSpeedX(velocity);

			Figure.setTexture(&resources.getTexture("Img/playerright.png"));

			this->Figure.move(speedx, speedy);
		}
		else if (Keyboard::isKeyPressed(Keyboard::Key::Up))
		{
			if (this->uppos() < 0)
			{
				return;
			}
			for (int i = 0; i < A.size(); i++)
			{
				if (A[i].leftpos() < rightpos() && A[i].uppos() < downpos() && A[i].downpos() > uppos() - velocity && A[i].rightpos() > leftpos())
				{
					if (!A[i].transpasstest() && A[i].getSymbol() != 's')
						return;
					if (A[i].getSymbol() == 's')
					{
						A[i].movefigure(A, this->getVelocity());
						if (A[i].CanBeEntered() == false)
							return;
					}
				}
			}
			this->setSpeedY(-velocity);

			Figure.setTexture(&resources.getTexture("Img/playerup.png"));

			this->Figure.move(speedx, speedy);
		}
		else if (Keyboard::isKeyPressed(Keyboard::Key::Down))
		{
			if (this->downpos() > Gameinfo::SCREENRESY)
			{
				return;
			}
			for (int i = 0; i < A.size(); i++)
			{
				if (A[i].leftpos() < rightpos() && A[i].uppos() < downpos() + velocity && A[i].downpos() > uppos() && A[i].rightpos() > leftpos())
				{
					if (!A[i].transpasstest() && A[i].getSymbol() != 's')
						return;
					if (A[i].getSymbol() == 's')
					{
						A[i].movefigure(A, this->getVelocity());
						if (A[i].CanBeEntered() == false)
							return;
					}
				}
			}

			{
				this->setSpeedY(velocity);

				Figure.setTexture(&resources.getTexture("Img/playerdown.png"));

				this->Figure.move(speedx, speedy);
			}
		}

		this->setSpeedX(0);
		this->setSpeedY(0);
	}
} //tylko dla ruchomych obiektow

void Object::movefigure(std::vector <Object> & A, int Vel)
{
	if (movable == true)
	{
		this->setVelocity(Vel);

		if (Keyboard::isKeyPressed(Keyboard::Key::Left))
		{
			if (this->leftpos() < 0)
			{
				this->enterable = false;
				this->movable = false;
				return;
			}
			for (int i = 0; i < A.size(); i++)
			{
				if (A[i].leftpos() < rightpos() && A[i].uppos() < downpos() && A[i].downpos() > uppos() && A[i].rightpos() > leftpos() - velocity)
					if (!A[i].transpasstest() && A[i].getSymbol() != 's')
					{
						this->enterable = false;
						this->movable = false;
						return;
					}
				if (lastmove == 0)
					lastmove = 1;
			}

			{


				if (lastmove == 1 && movable)
				{
					this->setSpeedX(-velocity);
					this->Figure.move(speedx, speedy);
				}
			}
		}
		else if (Keyboard::isKeyPressed(Keyboard::Key::Right))
		{
			if (this->rightpos() > float(Gameinfo::SCREENRESX - 190))
			{
				this->enterable = false;
				this->movable = false;
				return;
			}
			for (int i = 0; i < A.size(); i++)
			{
				if (A[i].leftpos() < rightpos() + velocity && A[i].uppos() < downpos() && A[i].downpos() > uppos() && A[i].rightpos() > leftpos())
					if (!A[i].transpasstest() && A[i].getSymbol() != 's')
					{
						this->enterable = false;
						this->movable = false;
						return;
					}
				if (lastmove == 0)
					lastmove = 2;
			}


			if (lastmove == 2 && movable)
			{
				this->setSpeedX(velocity);
				this->Figure.move(speedx, speedy);

			}
		}
		else if (Keyboard::isKeyPressed(Keyboard::Key::Up))
		{
			if (this->uppos() < 0)
			{
				this->enterable = false;
				this->movable = false;
				return;
			}
			for (int i = 0; i < A.size(); i++)
			{
				if (A[i].leftpos() < rightpos() && A[i].uppos() < downpos() && A[i].downpos() > uppos() - velocity && A[i].rightpos() > leftpos())
					if (!A[i].transpasstest() && A[i].getSymbol() != 's')
					{
						this->enterable = false;
						this->movable = false;
						return;
					}
				if (lastmove == 0)
					lastmove = 3;
			}


			if (lastmove == 3 && movable)
			{
				this->setSpeedY(-velocity);
				this->Figure.move(speedx, speedy);

			}
		}
		else if (Keyboard::isKeyPressed(Keyboard::Key::Down))
		{
			if (this->downpos() > Gameinfo::SCREENRESY)
			{
				this->enterable = false;
				this->movable = false;
				return;
			}
			for (int i = 0; i < A.size(); i++)
			{
				if (A[i].leftpos() < rightpos() && A[i].uppos() < downpos() + velocity && A[i].downpos() > uppos() && A[i].rightpos() > leftpos())
					if (!A[i].transpasstest() && A[i].getSymbol() != 's')
					{
						this->enterable = false;
						movable = false;
						return;
					}
				if (lastmove == 0)
					lastmove = 4;
			}


			if (lastmove == 4 && movable)
			{
				this->setSpeedY(velocity);
				this->Figure.move(speedx, speedy);
			}
		}

		this->setSpeedX(0);
		this->setSpeedY(0);
	}
}

void Object::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(Figure, states);
}

float Object::leftpos()
{
	return (Figure.getPosition().x - sizex / 2);
}

float Object::rightpos()
{
	return (Figure.getPosition().x + sizex / 2);
}

float Object::uppos()
{
	return (Figure.getPosition().y - sizex / 2);
}

float Object::downpos()
{
	return (Figure.getPosition().y + sizex / 2);
}
void Object::setMovable(bool Val)
{
	movable = Val;
}

int Object::getVelocity()
{
	return velocity;
}

bool Object::isvisible()
{
	return visible;
}

bool Object::isdestroyed()
{
	return destroyed;
}

void Object::changesize(float x, float y)
{
	sizex = x;
	sizey = y;
	Figure.setOrigin((sizex / 2), (sizey / 2));
	Figure.setSize({ x,y });

}

bool Object::CanBeEntered()
{
	return enterable;
}

void Object::destroy()
{
	destroyed = true;
}
