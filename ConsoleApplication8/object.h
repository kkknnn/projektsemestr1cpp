#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include "gameinfo.h"
#include "resourceholder.h"
using namespace sf;

class Object : public sf::Drawable    // dziedziczy po Drowable, funkcja draw
{
public:
	Object(int, int, float, char, bool);
	Object(); 
	virtual ~Object();
	
	float getPositionX();
	float getPositionY();
	void setPosition(float, float);


	char getSymbol();
	void setSpeedX(float);
	void setSpeedY(float);
	void setVelocity(float);
	void setTranspassable(bool);
	bool transpasstest();
	virtual void update();
	virtual void moveuser(std::vector <Object> & A);
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
	float leftpos();						//zwracaja pozycje kraw�dzi
	float rightpos();
	float uppos();
	float downpos();
	virtual void movefigure(std::vector <Object> & A, int Vel);
	void setMovable(bool);
	virtual void updateoncolision();
	int getLastMove();
	int getVelocity();
	bool isvisible();
	bool isdestroyed();
	void changesize(float x, float y);
	bool CanBeEntered();
	void destroy();
  

protected:
	sf::RectangleShape Figure;
	static Resources resources;
	
private:
	int lastmove = 0;
	bool movable=false;
	bool transpassing = true;
	bool visible = true;
	bool destroyed = false;
	bool enterable = true;
	char symbol;
	float sizex{ float(Gameinfo::FIGURESIZE) };
	float sizey{ float(Gameinfo::FIGURESIZE) };
	float speedx=0;
	float speedy=0;
	float velocity = 0;
	
};

