#include "pch.h"
#include "objectenemy.h"
#include <cstdlib>

ObjectEnemy::ObjectEnemy(int posx, int posy, char symbol) : Object(posx, posy, 1, symbol, true)
{

	if (symbol == 'v')
		this->changesize(Gameinfo::FIGURESIZE*0.95, Gameinfo::FIGURESIZE*0.95);
	if (symbol == 'x')
		this->changesize(Gameinfo::FIGURESIZE*0.85, Gameinfo::FIGURESIZE*0.85);
}

ObjectEnemy::ObjectEnemy() : Object(500, 400, 1, 'x', true)
{
}


ObjectEnemy::~ObjectEnemy()
{
}

void ObjectEnemy::updateoncolisionplayer()
{
	if (Gameinfo::WHATLEVEL() != 1)
	{
		if (oncollision == false)
		{
			if (this->getSymbol() == 'x')
				Gameinfo::HEALTHPOINTS -= 1;
			else
				Gameinfo::TIMEPENALTY += 10;
		}
	}
}

void ObjectEnemy::OnCollision()
{
	oncollision = true;
}

void ObjectEnemy::NotOnCollision()
{
	oncollision = false;
}

void ObjectEnemy::update(std::vector<Object>& A)
{
	if (getSymbol() == 'x')
		switch (PreviousMove)
		{
		case 0: Gameinfo::WHATLEVEL() != 1 ? Figure.setTexture(&resources.getTexture("Img/catleft.png")) : Figure.setTexture(&resources.getTexture("Img/ghostleft.png")); break;
		case 1: Gameinfo::WHATLEVEL() != 1 ? Figure.setTexture(&resources.getTexture("Img/catright.png")) : Figure.setTexture(&resources.getTexture("Img/ghostright.png")); break;
		case 2: Gameinfo::WHATLEVEL() != 1 ? Figure.setTexture(&resources.getTexture("Img/catup.png")) : Figure.setTexture(&resources.getTexture("Img/ghostup.png")); break;
		case 3: Gameinfo::WHATLEVEL() != 1 ? Figure.setTexture(&resources.getTexture("Img/catdown.png")) : Figure.setTexture(&resources.getTexture("Img/ghostdown.png")); break;
		default:break;
		}

	else
	{
		switch (PreviousMove)
		{
		case 0: Figure.setTexture(&resources.getTexture("Img/palleft.png")); break;
		case 1: Figure.setTexture(&resources.getTexture("Img/palright.png")); break;
		case 2: Figure.setTexture(&resources.getTexture("Img/palup.png")); break;
		case 3: Figure.setTexture(&resources.getTexture("Img/paldown.png")); break;
		default:break;
		}

	}


	for (int i = 0; i < 4; i++)
		MoveAvability[i] = true;          //zerowanie tablicy ruchow


	for (int i = 0; i < A.size(); i++)   // sprawdzanie vectora obiektow nieruchomych by ustalic czy mozna sie ruszyc
	{
		{
			if (A[i].leftpos() < rightpos() && A[i].uppos() < downpos() && A[i].downpos() > uppos() && A[i].rightpos() >= leftpos() - getVelocity())
			{
				MoveAvability[0] = false;
			}
			else if
				(leftpos() <= 0) MoveAvability[0] = false;
			//lewa
		}
		{
			if (A[i].leftpos() <= rightpos() + getVelocity() && A[i].uppos() < downpos() && A[i].downpos() > uppos() && A[i].rightpos() > leftpos())
			{
				MoveAvability[1] = false;
			}
			else if (this->rightpos() >= (Gameinfo::SCREENRESX - 190))
			{
				MoveAvability[1] = false; //prawa
			}

		}
		{
			if (A[i].leftpos() < rightpos() && A[i].uppos() < downpos() && A[i].downpos() >= uppos() - getVelocity() && A[i].rightpos() > leftpos())
			{
				MoveAvability[2] = false;
			}
			else if (uppos() <= 0) MoveAvability[2] = false; //gora
		}
		{
			if (A[i].leftpos() < this->rightpos() && A[i].uppos() <= this->downpos() + getVelocity() && A[i].downpos() > this->uppos() && A[i].rightpos() > leftpos())
			{
				MoveAvability[3] = false;
			}
			else if (downpos() + getVelocity() >= Gameinfo::SCREENRESY)
				MoveAvability[3] = false; // do�
		}
	}




	rand = std::rand() % 100;


	if (MoveAvability[PreviousMove] == true && rand < 95)
	{
		nextmove = PreviousMove;
	}
	else
	{
		/*for (int i = 0; i < 4; i++)
		{
			if (MoveAvability[i] == true)
				licznikprawd += 1;
		}*/


		if (MoveAvability[PreviousMove])    // zakaz cofania gdy obiekt moze poruszac sie do przodu i ma mozliwosc skretu
		{
			switch (PreviousMove)
			{
			case 0: if (MoveAvability[3] || MoveAvability[2]) MoveAvability[1] = false; break;
			case 1: if (MoveAvability[3] || MoveAvability[2]) MoveAvability[0] = false; break;
			case 2: if (MoveAvability[0] || MoveAvability[1]) MoveAvability[3] = false; break;
			case 3: if (MoveAvability[0] || MoveAvability[1]) MoveAvability[2] = false; break;
			default: break;
			}
		}
		nextmove = std::rand() % 4;
		/*	for (int i = 0; i < 4; i++)
			{
				if (MoveAvability[i] == false)
					continue;
					if (temp == rand)
						nextmove = i;
					else temp += 1;
			}*/
	}


	switch (nextmove)
	{
	case 0: if (MoveAvability[0] == true) {
		this->Figure.move(-getVelocity(), 0); PreviousMove = 0;
	} break;
	case 1: if (MoveAvability[1] == true) {
		this->Figure.move(getVelocity(), 0); PreviousMove = 1;
	}  break;
	case 2:	if (MoveAvability[2] == true) {
		this->Figure.move(0, -getVelocity()); PreviousMove = 2;
	}  break;
	case 3: if (MoveAvability[3] == true) {
		this->Figure.move(0, getVelocity()); PreviousMove = 3;
	} break;

	default:
		break;
	}



}



void ObjectEnemy::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(Figure, states);
}
