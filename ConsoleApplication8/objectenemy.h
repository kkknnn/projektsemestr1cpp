#pragma once
#include "object.h"

class ObjectEnemy: public Object
{
public:
	ObjectEnemy(int posx, int posy, char symbol);
	ObjectEnemy();
	~ObjectEnemy();
	virtual void updateoncolisionplayer();
	void OnCollision();
	void NotOnCollision();
	virtual void update(std::vector <Object> & A);
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
private:
	bool MoveAvability[5];
	int PreviousMove=0;
	int rand;
	int nextmove;
	bool oncollision = false;
};

