#include "pch.h"
#include "objectpointable.h"
#include <SFML/Graphics.hpp>
#include "gameinfo.h"

void ObjectPointable::UpdateOnCollision()
{
	if (!isdestroyed() && Gameinfo::WHATLEVEL() != 2)
	{
		this->destroy();

		if (getSymbol() == 'g')
		{
			Gameinfo::SCOREPOINT(1);
		}
		else if (getSymbol() == 'h')
		{
			Gameinfo::SETADDITIONALGOAL();
		}
		if (getSymbol() == 'k')
		{
			Gameinfo::SETKEYGATHERED();
		}
	}
	else if (!isdestroyed() && Gameinfo::WHATLEVEL() == 2)
	{
		if (!BeenUsed && getSymbol() == 'g')
		{
			BeenUsed = true;
			Gameinfo::SCOREPOINT(1);
			Figure.setTexture(&resources.getTexture("Img/comppointused.png"));

		}
		else if (getSymbol() == 'h')
		{

			Gameinfo::SETADDITIONALGOAL();
			Figure.setTexture(&resources.getTexture("Img/servenon.png"));
		}
		if (getSymbol() == 'k')
		{

			this->destroy();
			Gameinfo::SETKEYGATHERED();
		}
		if (getSymbol() == 'q')
			this->destroy();

	}


}
void ObjectPointable::UpdateOnCollisionEnemy()
{
	if (getSymbol() == 'g')
	{
		this->destroy();
		Gameinfo::SCOREPOINT(-1);
		Gameinfo::HEALTHPOINTS -= 1;
	}
}
void ObjectPointable::UpdateUserOnCollision(Object & Obj)
{
	if (this->getSymbol() == 'q')
	{
		Obj.setVelocity(3);
	}
}
void ObjectPointable::MoveObject(std::vector<Object>& A)
{
	this->moveuser(A);
}
void ObjectPointable::update()
{
}


ObjectPointable::ObjectPointable() :Object::Object(300, 300, 1, 'g', false),
BeenUsed{ false }
{

}

ObjectPointable::ObjectPointable(int a, int b, float c, char d, bool e) :
	Object::Object(a, b, c, d, e)

{
	if (Gameinfo::WHATLEVEL() == 0 && getSymbol() == 'g')
		Figure.setTexture(&resources.getTexture("Img/floppy-disk.png"));
	else if (Gameinfo::WHATLEVEL() == 0 && getSymbol() == 'h')
		Figure.setTexture(&resources.getTexture("Img/floppy-disk-system.png"));
	else if (Gameinfo::WHATLEVEL() == 2 && getSymbol() == 'h')
		Figure.setTexture(&resources.getTexture("Img/server.png"));
	else if (getSymbol() == 'k')
		Figure.setTexture(&resources.getTexture("Img/key.png"));
	else if (Gameinfo::WHATLEVEL() == 1 && getSymbol() == 'g')
	{
		Figure.setTexture(&resources.getTexture("Img/cpp.png"));
	}
	else if (getSymbol() == 'q')
	{
		Figure.setTexture(&resources.getTexture("Img/coffee.png"));
	}

	else if (Gameinfo::WHATLEVEL() == 2 && getSymbol() == 'g')
	{
		Figure.setTexture(&resources.getTexture("Img/comppoint.png"));
	}
	else
		Figure.setTexture(&resources.getTexture("Img/floppy-disk-system.png"));
}

ObjectPointable::~ObjectPointable()
{
}

void ObjectPointable::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	target.draw(Figure, states);
}
