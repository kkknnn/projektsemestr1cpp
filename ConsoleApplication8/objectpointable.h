#pragma once
#include "object.h"
class ObjectPointable : public Object
{
	
public:
	ObjectPointable();
	ObjectPointable(int, int, float, char, bool);
	void UpdateOnCollision();
	void UpdateOnCollisionEnemy();
	void UpdateUserOnCollision(Object &);
	void MoveObject(std::vector <Object> & A);
	~ObjectPointable();
	void update();
	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;
private:
	bool BeenUsed;
};

