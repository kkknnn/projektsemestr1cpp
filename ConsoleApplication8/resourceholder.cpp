#include "pch.h"
#include "resourceholder.h"


Resources::Resources()
{
}


Resources::~Resources()
{
}

const sf::Texture & Resources::getTexture(std::string adress)
{
	auto temp = textures.find(adress); //metoda find dla map, jesli nie znajdzie obiektu zwraca refencje do konca map.
	if (temp != textures.end())
		return temp->second;   // second to druga sk�adowa elementu mapy, w tym wypadku textura;
	else
	{
		sf::Texture temp;
		temp.loadFromFile(adress);
		textures.insert(std::make_pair(adress, temp));
		auto temp2 = textures.find(adress);
		return temp2->second;
	}
}

const sf::IntRect & Resources::getAnimation(int row)
{
	int frame = ((starttime - clock()) / (CLOCKS_PER_SEC * 2)) % 3;
	sf::IntRect intrect(frame * 150, row * 150, 150, 150);
	return intrect;
}

/*const sf::IntRect & Resources::getAnimation(std::string adress, int row)
{
	{

		auto temp = SpriteSheets.find(adress); //metoda find dla map, jesli nie znajdzie obiektu zwraca refencje do konca map.
		if (temp != SpriteSheets.end())
		{
			sf::Texture temp2 = temp->second;  // second to druga sk�adowa elementu mapy, w tym wypadku textura;

			intrect.
				return temp->second;
		}
		else
		{
			sf::Texture temp;
			temp.loadFromFile(adress);
			SpriteSheets.insert(std::make_pair(adress, temp));
			return temp;
		}
	}
}*/
