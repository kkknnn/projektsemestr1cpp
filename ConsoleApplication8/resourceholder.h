#pragma once
#include <SFML/Graphics.hpp>
class Resources
{
	std::map <std::string, sf::Texture> textures;
	//std::map <std::string, sf::Texture> SpriteSheets;

public:
	Resources();
	~Resources();
	const sf::Texture & getTexture(std::string);
    const sf::IntRect & getAnimation(int);
private:
   int starttime = clock();
};

